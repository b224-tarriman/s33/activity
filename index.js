fetch('https://jsonplaceholder.typicode.com/todos',{
    method:'GET',
    headers:{
        'Content-Type': 'application/json'
    }
}).then((res)=>res.json())
.then((arr)=> {
   arr.map((elem)=>{
    let newArr = []
    newArr.push(elem.title)
    console.log(newArr)
   })
})


fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method:'GET',
    headers:{
        'Content-Type': 'application/json'
    }
}).then((res)=>res.json())
.then((arr)=> console.log(`The item "${arr.title}" on the list has a status of ${arr.completed}`))

fetch('https://jsonplaceholder.typicode.com/todos',{
    method:'POST',
    headers:{
        'Content-Type': 'application/json'
    },
    body:JSON.stringify({
        "userId": 1,
        "id": 1,
        "title": "Created To Do List Item",
        "completed": false
    })

}) .then ((res)=> res.json())
.then((arr)=>console.log(arr))

fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method:'PUT',
    headers:{
        'Content-Type': 'application/json'
    },
    body:JSON.stringify({
        "dateCompleted": "pending",
        "description":"To update my to do list with a different data structure",
        "userId": 1,
        "id": 1,
        "title": "Updated To Do List Item",
        "status": "pending"
    })

}) .then ((res)=> res.json())
.then((arr)=>console.log(arr))

fetch('https://jsonplaceholder.typicode.com/todos/1',{
    method:'PATCH',
    headers:{
        'Content-Type': 'application/json'
    },
    body:JSON.stringify({
        "dateCompleted": "11/29/2022",
        "status": "Complete"
    })

}) .then ((res)=> res.json())
.then((arr)=>console.log(arr))

fetch('https://jsonplaceholder.typicode.com/todos/2',{
    method:'DELETE'
})

